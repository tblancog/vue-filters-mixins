import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', (value) => {
    return value.toLowerCase();
});
Vue.filter('length', (value) => {
  return value + " ("+ value.length +")"
})
// Vue.filter('capitalize', (value)=> {
//   return value.toString().charAt(0).toUpperCase()+ value.slice(1)
// })

Vue.mixin({
    created() {
        console.log('Global Mixin - Created Hook');
    }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
