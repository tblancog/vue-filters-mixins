export const computedMixin = {
  computed: {
    reversed() {
      return this.text.split('').reverse().join('')
    }
  }
};
